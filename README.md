# stem-cli-sender
Util app to send messages from command line interface.

## Usage
```bash
./stem-cli-sender --channel test "message content"
```

## Building from scratch
```bash
cargo build --release
```
