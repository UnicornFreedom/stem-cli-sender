#[macro_use]
extern crate clap;

use bytes::{BytesMut, BufMut};

use std::io::prelude::*;
use std::net::TcpStream;

#[derive(Clap)]
#[clap(version = "0.1.0", author = "Totoro")]
struct Opts {
    /// Sets the address of STEM server
    #[clap(short = "s", long = "server", default_value = "stem.fomalhaut.me:5733")]
    server: String,
    /// Sets the name of the target channel
    #[clap(short = "c", long = "channel")]
    channel: String,
    /// The message to be sended
    message: String
}

fn main() -> std::io::Result<()>{
    let opts: Opts = Opts::parse();

    let mut bytes = BytesMut::new();
    let len = 2 + opts.channel.len() + opts.message.len();
    bytes.reserve(2 + len);
    bytes.put_u16(len as u16);
    bytes.put_u8(0);
    bytes.put_u8(opts.channel.len() as u8);
    bytes.put_slice(opts.channel.as_ref());
    bytes.put_slice(opts.message.as_ref());

    let mut stream = TcpStream::connect(opts.server)?;
    stream.write(&bytes)?;
    stream.flush()?;

    //println!("Packet: [{:?}]", bytes);

    Ok(())
}
